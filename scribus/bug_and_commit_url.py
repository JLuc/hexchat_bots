#!/usr/bin/python
# -*- coding: iso-8859-15 -*-
# changelog :
# v0.4 : prints localy some messages instead of sending them to the chan  (ale's request)
          
__module_name__ = "Scribus bugs and commits tracker"
__module_version__ = "0.4 (20/06/2018)"
__module_description__ = "Turns a short bug #1234 into a real url http://bugs.scribus.net/view.php?id=1234 and a 'revision 1234' into a real url http://scribus.net/websvn/comp.php?repname=Scribus&compare[]=%2F@1233&compare[]=%2F@1234 - except for mrscribe"
__author__ = "JLuc - "

from html.parser import HTMLParser
import os
import re
import requests
import threading
import urllib3
import hexchat

events = ("Channel Message", 
# "Channel Action",
#        "Channel Msg Hilight", "Channel Action Hilight",
          "Private Message", "Private Message to Dialog",
#          "Private Action", "Private Action to Dialog",
          "Message send", "Your Message")

# including private messages
scribus_watched_channels = ("#scribus", "#scribus-test", "#scribus-dev", "#jluc")

def explain_scribus(word, word_eol, userdata, attr=""):
	chan = hexchat.get_info("channel")
	if chan in scribus_watched_channels:

		found = re.search(".*r[éeè]vision\s*([0-9]+)($|[^0-9].*)", word[1], re.IGNORECASE)
		if found:
			revnum = int(found.group(1))
			hexchat.command ("say * yeah ! http://scribus.net/websvn/comp.php?repname=Scribus&compare[]=%2F@"+str(revnum-1)+"&compare[]=%2F@"+str(revnum)+" (wait a bit when baking page)")

		found = re.search(".*#([0-9]+)($|[^0-9].*)", word[1], re.IGNORECASE)
		if found:
			if int(found.group(1)) > 1000:
				if (word[0] != 'mrscribe'):
					# say envoie un texte sur le chan, visible par tous, avec le nick de la personne chez qui le script est exécuté
					hexchat.command("say * hop ! http://bugs.scribus.net/view.php?id="+ found.group(1))
				else:
					# print affiche seulement pour le client chez qui le script est exécuté, sans nick
					print("* voilà jluc : http://bugs.scribus.net/view.php?id="+ found.group(1))

	# treat event as usual after callback
	return hexchat.EAT_NONE

for event in events:
    # hexchat.command("say hook "+event) 
    hexchat.hook_print(event, explain_scribus)

# prnt : que moi voit / say : tout le monde voit comme si je parlais
hexchat.prnt("===")
hexchat.prnt(__module_name__ + " version " + __module_version__ + " loaded")
