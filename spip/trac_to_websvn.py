#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

__module_name__ = "SPIP irc bot"
__module_version__ = "0.7 (04/12/2019)"
__module_description__ = "On #spip : converts trac urls to websvn urls"
__author__ = "JLuc"

from html.parser import HTMLParser
import os
import re
import requests
import threading
import urllib3
import hexchat

events = ("Channel Message", 
# "Channel Action",
#        "Channel Msg Hilight", "Channel Action Hilight",
          "Private Message", "Private Message to Dialog",
#          "Private Action", "Private Action to Dialog",
          "Message send", "Your Message")

spip_watched_channels = ("#spip", "#jluc")

# Pas de traduction vers git car il faut un hash
# https://git.spip.net/plugin/statistiques_objets/commit/0a33d019526b777ac4d11594223289241bcb797d

ephactiv = True

def explain_spip(word, word_eol, userdata, attr=""):

	global ephactiv
	more = "Tu peux aussi demander 'Eph où es ton code ?'"
	if re.search("^Eph (help|-?\?).*", word[1], re.IGNORECASE) :
		if ephactiv :
			hexchat.command ("say Eph dit « Là je suis activ. Demande 'Eph stop' et je m'inactiv... "+ more + " »")
		else :
			hexchat.command ("say Eph dit « Là je suis inactiv. Demande 'Eph go' et je m'activ... "+ more + " »")

	elif re.search("^Eph stop.*", word[1], re.IGNORECASE) :
		ephactiv = False
		hexchat.command ("say Eph dit « Ok je m'inactiv »")
		
	elif re.search("^Eph go.*", word[1], re.IGNORECASE) :
		ephactiv = True
		hexchat.command ("say Eph dit « Ok je m'activ »")

	elif ephactiv :
		# spip:  http://core.spip.org/projects/spip/repository/revisions/24438 (1 files)
		found = re.search(".*core.spip.org/projects/spip/repository/revisions/([0-9]+)([^0-9]|$).*", word[1], re.IGNORECASE)
		if found:
			revnum = int(found.group(1))
			# hexchat.command ("say * ou bien là : https://websvn.spip.net/comp.php?repname=Zone&compare[]=%2F@"+str(revnum-1)+"&compare[]=%2F@"+str(revnum))
			hexchat.command ("say * ou bien là : https://websvn.spip.net/revision.php?repname=Zone&path=%2F&rev="+str(revnum)+"&peg="+str(revnum))

		# shiraz> spip-zone:  https://zone.spip.net/trac/spip-zone/changeset/118029 (1 files) : ***message de log***
		# donne le diff : https://websvn.spip.net/comp.php?repname=Zone&compare[]=%2F@118261&compare[]=%2F@118262
		# donne le log :  https://websvn.spip.net/revision.php?repname=Zone&path=%2F&rev=118268&peg=118268
		found = re.search(".*trac/spip-zone/changeset/([0-9]+)([^0-9]|$).*", word[1], re.IGNORECASE)
		if found:
			revnum = int(found.group(1))
			# hexchat.command ("say * ou bien là : https://websvn.spip.net/comp.php?repname=Zone&compare[]=%2F@"+str(revnum-1)+"&compare[]=%2F@"+str(revnum))
			hexchat.command ("say * ou bien là : https://websvn.spip.net/revision.php?repname=Zone&path=%2F&rev="+str(revnum)+"&peg="+str(revnum))

		# Lien vers une url source : https://zone.spip.org/trac/spip-zone/browser/spip-zone/_plugins_/gis/trunk/gis_fonctions.php#L415
		# doit devenir : https://websvn.spip.net/filedetails.php?repname=Zone&path=%2F_plugins_%2Fgis%2Ftrunk%2Fgis_fonctions.php
		# (pas de n° de ligne avec websvn)
		found = re.search(".*trac/spip-zone/browser/spip-zone([^\b#]*)(#L\d*)?(.*)$", word[1], re.IGNORECASE)
		if found:
			chemin = found.group(1)
			# On pourrait traduire vers git mais tous les plugins sont pas sur git.spip.net
			hexchat.command ("say * ou bien là : https://websvn.spip.net/filedetails.php?repname=Zone&path="+chemin)

		found = re.search("^Eph[^\w](.*)(t(u|')? ?est? o[ùu]|c'est quoi t|o[ùu] est? t).*\?.*", word[1], re.IGNORECASE)
		if found:
			hexchat.command ("say Eph dit « Chuis là : https://gitlab.com/JLuc/hexchat_bots/tree/master/spip »")
			
		else :
			found = re.search("^Eph[^\w](.*)\?.*", word[1], re.IGNORECASE)
			# troll
			if found:
				what = found.group(1)
				hexchat.command ("say Eph dit « Ouep, " + what + " un temps »")

	# treat event as usual after callback
	return hexchat.EAT_NONE

for event in events:
    # hexchat.command("say hook "+event) 
	chan = hexchat.get_info("channel")
	if chan in spip_watched_channels:
		hexchat.hook_print(event, explain_spip)

# prnt : ya que moi le voit 
# say : tout le monde le voit comme si c'était moi qui l'écrivait
hexchat.prnt("===")
hexchat.prnt(__module_name__ + " version " + __module_version__ + " loaded")

